﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;

public class StairRotation : MonoBehaviour
{
    public OVRInput.Controller controller;
    public Animator upperStairAnim;

    private bool isPlaying = false;
    private IEnumerator resetFlagCorutinne;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        if (!isPlaying && (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, controller) > 0 || Input.GetKeyDown(KeyCode.A))) {
            upperStairAnim.SetTrigger("Play");
            isPlaying = true;

            if (resetFlagCorutinne != null)
                StopCoroutine(resetFlagCorutinne);

            resetFlagCorutinne = ResetFlag();
            StartCoroutine(resetFlagCorutinne);
        }     
    }

    IEnumerator ResetFlag() {
        yield return new WaitForSeconds(1);
        isPlaying = false;
    }
}
